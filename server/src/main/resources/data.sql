DROP TABLE user;

CREATE TABLE user (
  id INT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(32) NOT NULL,
  password VARCHAR(256) NOT NULL
);

INSERT INTO user (username, password) VALUES ('felipe.thiago', 'teste');