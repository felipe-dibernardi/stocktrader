package br.com.fdbst.stocktrader.dto;

/**
 * Classe AuthenticationResponseDTO
 * <p>
 * Essa classe é responsável por...
 *
 * @author Felipe Di Bernardi S Thiago
 */
public class AuthenticationResponseDTO {

    private String token;

    public AuthenticationResponseDTO() {
    }

    public AuthenticationResponseDTO(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
