package br.com.fdbst.stocktrader.repository;

import br.com.fdbst.stocktrader.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface UserRepository
 * <p>
 * Essa intereface é responsável por...
 *
 * @author Felipe Di Bernardi S Thiago
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsernameAndPassword(String username, String password);

}
