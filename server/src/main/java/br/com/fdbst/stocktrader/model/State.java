package br.com.fdbst.stocktrader.model;

import lombok.Data;
import java.math.BigDecimal;
import java.util.List;

/**
 * Classe State
 * <p>
 * Essa classe é responsável por...
 *
 * @author Felipe Di Bernardi S Thiago
 */
@Data
public class State {

    private BigDecimal funds;

    private List<Stock> stocks;

    private List<Portfolio> portfolios;

    private User user;

    public State() {
    }

    public State(BigDecimal funds, List<Stock> stocks, List<Portfolio> portfolios, User user) {
        this.funds = funds;
        this.stocks = stocks;
        this.portfolios = portfolios;
        this.user = user;
    }
}
