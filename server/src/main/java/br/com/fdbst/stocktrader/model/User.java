package br.com.fdbst.stocktrader.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Classe User
 * <p>
 * Essa classe é responsável por...
 *
 * @author Felipe Di Bernardi S Thiago
 */
@Data
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String username;

    private String password;

    @Transient
    private State state;

    public User() {
    }

    public User(String username, String password, State state) {
        this.username = username;
        this.password = password;
        this.state = state;
    }
}
