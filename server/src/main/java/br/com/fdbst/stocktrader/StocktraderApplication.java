package br.com.fdbst.stocktrader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StocktraderApplication {

    public static void main(String[] args) {
        SpringApplication.run(StocktraderApplication.class, args);
    }

}
