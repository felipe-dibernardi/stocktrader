package br.com.fdbst.stocktrader.controller;

import br.com.fdbst.stocktrader.model.State;
import org.springframework.web.bind.annotation.*;

/**
 * Classe StateController
 * <p>
 * Essa classe é responsável por...
 *
 * @author Felipe Di Bernardi S Thiago
 */
@RestController
@CrossOrigin
public class StateController {

    private State state;

    @GetMapping("/api/data")
    public State getState() {
        return this.state;
    }

    @PutMapping("/api/data")
    public void setState(@RequestBody State state) {
        this.state = state;
    }

}
