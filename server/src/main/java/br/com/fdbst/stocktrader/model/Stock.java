package br.com.fdbst.stocktrader.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Classe Stock
 * <p>
 * Essa classe é responsável por...
 *
 * @author Felipe Di Bernardi S Thiago
 */
@Data
public class Stock {

    private String name;

    private BigDecimal value;

    public Stock() {
    }

    public Stock(String name, BigDecimal value) {
        this.name = name;
        this.value = value;
    }
}
