package br.com.fdbst.stocktrader.model;

import lombok.Data;


/**
 * Classe Portfolio
 * <p>
 * Essa classe é responsável por...
 *
 * @author Felipe Di Bernardi S Thiago
 */
@Data
public class Portfolio {

    private Integer quantity;

    private Stock stock;

    public Portfolio() {
    }

    public Portfolio(Integer quantity, Stock stock) {
        this.quantity = quantity;
        this.stock = stock;
    }

}
