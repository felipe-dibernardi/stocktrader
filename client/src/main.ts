import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import axios, { AxiosRequestConfig } from 'axios';

Vue.config.productionTip = false;

axios.defaults.baseURL = "http://localhost:8080/api"

axios.interceptors.request.use((config: AxiosRequestConfig) => {
  if (store.getters['authentication/token']) {
    config.headers['Authorization'] = 'Bearer ' + store.getters['authentication/token'];
  }
  return config;
});

Vue.filter('currency', (value: number) => {
  return '$' + value.toLocaleString();
})

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
