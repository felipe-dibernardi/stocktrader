import Axios from "axios";
import State from '@/model/State';

class StateService {

    constructor() {}

    fetchData() {
        return Axios.get<State>('/data');
    }

    saveData(state: State) {
        return Axios.put('/data', state);
    }

}

export const stateService = new StateService();