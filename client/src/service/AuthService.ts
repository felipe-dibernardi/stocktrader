import Axios from "axios";
import AuthCredential from '@/model/AuthCredential';

class AuthService {

    constructor() {}

    login(credentials: AuthCredential) {
        return Axios.post('/authenticate', credentials);
    }

}

export const authService = new AuthService();