import { Stock } from './Stock';

export class Portfolio {
    constructor(public stock: Stock, public quantity: number) {}
}