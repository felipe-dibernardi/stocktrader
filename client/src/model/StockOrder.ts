import { Stock } from './Stock';

export class StockOrder {
    constructor(public stock: Stock, public quantity: number) {}
}