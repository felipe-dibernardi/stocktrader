export default class AuthCredential {
    constructor(public username: string, public password: string) {}
}