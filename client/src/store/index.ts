import Vue from 'vue';
import Vuex, {StoreOptions, GetterTree, MutationTree, ActionTree} from 'vuex';
import { RootState } from './types';
import { stocks } from './stocks/index';
import { portfolios } from './portfolios/index';
import { authentication } from './authentication/index'
import { stateService } from '@/service/StateService';
import State from '@/model/State';


Vue.use(Vuex);

const getters: GetterTree<RootState, RootState> = {
    funds (state) {
        return state.funds;
    }
}

const actions: ActionTree<RootState, RootState> = {
    addFunds(context, funds: number) {
        context.commit('setFunds', context.state.funds + funds);
    },
    removeFunds(context, funds: number) {
        context.commit('setFunds', context.state.funds - funds);
    },
    loadData(context) {
        stateService.fetchData()
        .then(resp => {
            if (resp.data) {
                context.commit('setFunds', resp.data.funds);
                if (resp.data.stockPortfolio) {
                    context.commit('portfolios/setPortfolio', resp.data.stockPortfolio);
                }
                if (resp.data)
                context.commit('stocks/setStocks', resp.data.stocks);
            }
            
        })
        .catch(err => console.log(err));
    },
    saveData(context, state: State) {
        stateService.saveData(state)
        .then(resp => {
            if (resp.status === 200) {
                alert('State saved successfully');
            }
        })
        .catch(err => {
            alert('Error on saving state. Check log for details.');
            console.log(err);
        })
    }    
}

const mutations: MutationTree<RootState> = {
    setFunds(state, funds) {
        state.funds = funds;
    }
}

const store: StoreOptions<RootState> = {
    state: {
        funds: 10000
    },
    getters,
    mutations,
    actions,
    modules: {
        stocks,
        portfolios,
        authentication
    }
}



export default new Vuex.Store<RootState>(store);