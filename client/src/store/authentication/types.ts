export interface AuthenticationState {
    token: string,
    username: string,
    expirationDate: Date | null
}