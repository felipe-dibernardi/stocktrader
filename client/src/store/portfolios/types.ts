import { Portfolio } from '@/model/Portfolio';

export interface PortfolioState {
    portfolios: Portfolio[]
}