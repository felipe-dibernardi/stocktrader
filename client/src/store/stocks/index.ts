import { StocksState } from './types';
import { Module, MutationTree, ActionTree, GetterTree } from 'vuex';
import { RootState } from '../types';
import { Stock } from '@/model/Stock';
import { StockOrder } from '@/model/StockOrder';

const state: StocksState = {
    stocks: []
};


const getters: GetterTree<StocksState, RootState> = {
    stocks: (state: StocksState) => {
      return state.stocks;
    }
}

const actions: ActionTree<StocksState, RootState> = {
    buyStocks: (context, order: StockOrder) => {
      context.commit('portfolios/buyStock', order, { root: true });
      context.commit('setFunds', context.rootGetters.funds - (order.stock.value * order.quantity), { root: true })
    },
    initStocks: (context) => {
      context.commit('setStocks', context.state.stocks.length === 0 ? [
      new Stock('BMW', 100),
      new Stock('Apple', 200),
      new Stock('Google', 175),
      new Stock('Twitter', 50)
      ] : context.state.stocks);
    },
    randomizeStocks: (context) => {
      context.commit('randomStocks');
      context.commit('portfolios/updatePortfolios', context.state.stocks, { root: true });
    }
}

const mutations: MutationTree<StocksState> = {
    setStocks(state: StocksState, payload: Stock[]) {
      state.stocks = payload;
    },
    randomStocks(state: StocksState) {
      state.stocks.forEach(stock => {
        stock.value = Math.floor(Math.random() * stock.value + stock.value/ 2);
      });
    }
}

const namespaced: boolean = true;

export const stocks: Module<StocksState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations
}