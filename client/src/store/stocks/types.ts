import { Stock } from '@/model/Stock';

export interface StocksState {
    stocks: Stock[];
}